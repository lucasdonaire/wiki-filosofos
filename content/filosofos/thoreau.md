+++
title = 'Thoreau'
date = 2023-10-02T19:02:04-03:00
draft = false
+++
![Thoreau](https://th.bing.com/th/id/OIP.d7_bj0r7UhQS0vasr5QyewAAAA?w=171&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7)
![Thoreau](https://th.bing.com/th/id/OIP.KpIupv1Be13FIynu5cPi9gHaJK?pid=ImgDet&rs=1)
![Thoreau](https://media.npr.org/assets/img/2014/02/13/ap97090803139_custom-f85c0e1fbf068caa157f61baa869c7bdb76db94b-s1400.jpg)
## Sobre
Henry David Thoreau (Concord, 12 de julho de 1817 — Concord, 6 de maio de 1862) foi um autor estadunidense, poeta, naturalista, pesquisador, historiador, filósofo e transcendentalista. Ele é mais conhecido por seu livro Walden, uma reflexão sobre a vida simples cercada pela natureza, e por seu ensaio A Desobediência Civil.
Os livros, ensaios, artigos, jornais e poesias de Thoreau chegam a mais de 20 volumes. Entre suas contribuições mais influentes encontravam-se seus escritos sobre história natural e filosofia, onde ele antecipou os métodos e preocupações da ecologia e do ambientalismo. Seu estilo de escrita literária intercala observações naturais, experiência pessoal, retórica pontuada, sentidos simbolistas, e dados históricos; ao mesmo tempo em que evidencia grande sensibilidade poética, austeridade filosófica, e uma paixão "yankee" pelo detalhe prático. Ele também era profundamente interessado na ideia de sobrevivência face a contextos hostis, mudança histórica, e decadência natural; ao mesmo tempo em que buscava abandonar o desperdício e a ilusão de forma a descobrir as verdadeiras necessidades essenciais da vida.
## Livros
- A Desobediência Civil
- Walden