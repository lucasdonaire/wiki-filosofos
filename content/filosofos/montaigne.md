+++
title = 'Montaigne'
date = 2023-10-02T19:02:11-03:00
draft = false
+++

![Montaige](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Montaigne_vers_1590_-_portrait_anonyme.jpg/558px-Montaigne_vers_1590_-_portrait_anonyme.jpg)
![Montaige](https://parliamentofdreams.files.wordpress.com/2012/03/montaigne415.jpg)
![Montaige](https://th.bing.com/th/id/OIP.PWb1rYMsGEnLKy_ZplTxWgHaG_?pid=ImgDet&rs=1)

## Sobre
Michel Eyquem de Montaigne (Castelo de Montaigne, 28 de fevereiro de 1533 — Castelo de Montaigne, 13 de setembro de 1592), mais conhecido apenas como Montaigne foi um filósofo renascentista e escritor erudito francês. Humanista e cético, ele é considerado como o precursor do estilo literário ensaístico. Empregando em sua obra um estilo descontínuo até então inédito na prosa literária, Montaigne refletiu sobre os costumes e modos de vida humanos, inaugurando assim o chamado moralismo francês.
Criticou a educação livresca e mnemônica, propondo um ensino voltado para a experiência e para a ação. Acreditava que a educação livresca exigiria muito tempo e esforço, o que afastaria os jovens dos assuntos mais urgentes da vida. Para ele, a educação deveria formar indivíduos aptos ao julgamento, ao discernimento moral e à vida prática.
## Livros
- Ensaios
- A educação das crianças