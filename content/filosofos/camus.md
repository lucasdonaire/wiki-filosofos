+++
title = 'Camus'
date = 2023-10-02T19:01:59-03:00
draft = false
+++

![Camus](https://th.bing.com/th/id/R.339702734b40ed75b1e19e586f2f22d3?rik=ZnUOtQa3D%2fLbuw&riu=http%3a%2f%2f3.bp.blogspot.com%2f-jVMm7dleaa4%2fTeQAdh4d6JI%2fAAAAAAAAANw%2fIpzOCKALvM8%2fs320%2falbert-camus-0011.jpg&ehk=RG0VeCdaAXAwxdvSftt3SL4P4TOnUA0KFgwOIbuQCMc%3d&risl=&pid=ImgRaw&r=0)
![Camus](https://th.bing.com/th/id/R.c28e5e4c175209a4a555ab5012e3ae72?rik=GnSBtAtMjYMnqw&pid=ImgRaw&r=0)
![Camus](https://www.thefamouspeople.com/profiles/images/albert-camus-13.jpg)

## Sobre
Albert Camus (Mondovi, 7 de novembro de 1913 – Villeblevin, 4 de janeiro de 1960) foi um escritor, filósofo, romancista, dramaturgo, jornalista e ensaísta franco-argelino. Ele também atuou como jornalista militante envolvido na Resistência Francesa, situando-se próximo das correntes libertárias durante as batalhas morais no período pós-guerra. O seu trabalho profícuo inclui peças de teatro, novelas, notícias, filmes, poemas e ensaios, onde ele desenvolveu um humanismo baseado na consciência do absurdo da condição humana e na revolta como uma resposta a esse absurdo. Para Camus, essa revolta leva à ação e fornece sentido ao mundo e à existência. Daqui "Nasce então a estranha alegria que nos ajuda a viver e a morrer". Recebeu o Prémio Nobel de Literatura em 1957.
## Livros
- O Mito de Sísifo
- A Peste
- O homem revoltado
- A queda