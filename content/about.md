+++
title = 'Sobre'
date = 2023-10-02T18:28:09-03:00
draft = false
+++

Esse é um site com intuito educativo. Seu criador, Lucas Donaire, é um estudante com muitos interesses, que acha que o conhecimento é essencial para uma vida bem vivida.